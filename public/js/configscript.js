window.onload = function () {
	window.Twitch.ext.onAuthorized(function (auth) {
		var config = {
			container: document.getElementById('config-canvas'),
			radius: 10,
			maxOpacity: .8,
			minOpacity: 0,
			blur: .5
		}
		var buffer = new Worker('js/configbuffer.js')
		$('#config-canvas').width($('#config-map').width())
		$('#config-canvas').height($('#config-map').height())
		buffer.postMessage({ width: $('#config-canvas').width() })
		buffer.postMessage({ streamer: auth.channelId })
		var heatmapInstance = h337.create(config);
		buffer.onmessage = function (e) {
			heatmapInstance.setData(e.data)
		}
		$(window).resize(function () {
			$('#config-canvas').width($('#config-map').width())
			$('#config-canvas').height($('#config-map').height())
			heatmapInstance.setDimensions($('#config-canvas').width(), $('#config-canvas').height())
			buffer.postMessage({ width: $('#config-canvas').width() })
		})
	});
};