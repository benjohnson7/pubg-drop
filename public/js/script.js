window.onload = function () {
	window.Twitch.ext.onAuthorized(function (auth) {
		var config = {
			container: document.getElementById('plotcanvas'),
			radius: 10,
			maxOpacity: .8,
			minOpacity: 0,
			blur: .5
		};
		var buffer = new Worker('js/buffer.js')
		$('#plotcanvas').width($('#pubg-map').width())
		$('#plotcanvas').height($('#pubg-map').height())
		buffer.postMessage({ width: $('#plotcanvas').width() })
		buffer.postMessage({ streamer: auth.channelId })
		var heatmapInstance = h337.create(config);
		buffer.onmessage = function (e) {
			heatmapInstance.setData(e.data)
		}
		$(window).resize(function () {
			$('#plotcanvas').width($('#pubg-map').width());
			$('#plotcanvas').height($('#pubg-map').height());
			heatmapInstance.setDimensions($('#plotcanvas').width(), $('#plotcanvas').height())
			buffer.postMessage({ width: $('#plotcanvas').width() })
		});
		var timeout = false
		var fb = firebase.database().ref();
		$('#plotcanvas').contextmenu(function (e) {
			e.preventDefault()
			if (!timeout) {
				timeout = true
				setTimeout(function () {
					timeout = false
				}, 200)
				let uuid = uuidv4()
				let click = fb.child(auth.channelId).child("clicks").push()
				console.log('left offset ' + $('#plotcanvas').offset().left)
				console.log('top offset ' + $('#plotcanvas').offset().top)
				console.log('width ' + $('#plotcanvas').width())
				click.set({
					x: (e.pageX - $('#plotcanvas').offset().left) / $('#plotcanvas').width(),
					y: (e.pageY - $('#plotcanvas').offset().top) / $('#plotcanvas').width(),
					key: uuid
				});
			}
		})
		$('#toggle-button').click(function () {
			$('#pubg-map').toggleClass('hidden')
			$('#plotcanvas').toggleClass('hidden')
			$('#toggle-button').toggleClass('disabled')
			$('#toggle-info').css('display', 'none')
		});
		$('#toggle-close').click(function () {
			$('#toggle-info').css('display', 'none')
			$('#toggle-close').toggle()
		});
	});
};