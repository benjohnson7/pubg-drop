importScripts('firebase-source.js', 'firebase.js')
var heatmapPoints = []
var width;
function setPoints(id){
	var streamerDb = firebase.database().ref().child(id).child("clicks");
	streamerDb.on('child_added', function (data) {
		let point = {
			x: data.child('x').val(),
			y: data.child('y').val(),
			value: 20,
			key: data.child('key').val()
		}
		heatmapPoints.push(point)
		sendPoints(heatmapPoints)
	});
	streamerDb.on('child_removed', function (data) {
		for (i = 0; i < heatmapPoints.length; i++) {
			if (heatmapPoints[i].key == data.child('key').val()) {
				heatmapPoints.splice(i, 1);
			}
		}
		sendPoints(heatmapPoints)
	});
}
function sendPoints(pointArr) {
	scaledPoints = JSON.parse(JSON.stringify(pointArr))
	for (var i = 0; i < pointArr.length; i++) {
		scaledPoints[i].x = Math.round(pointArr[i].x * width)
		scaledPoints[i].y = Math.round(pointArr[i].y * width)
	}
	var points = {
		max: 100,
		min: 0,
		data: scaledPoints
	}
	postMessage(points)
	scaledPoints = {}
}
onmessage = function(e) {
	if (e.data.width) {
		width = e.data.width
		sendPoints(heatmapPoints)
	} else if (e.data.streamer) {
		setPoints(e.data.streamer)
	}
}