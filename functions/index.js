const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

exports.autoDelete = functions.database.ref('/{streamerId}/clicks/{clickId}')
.onCreate(event => {
	setTimeout(function(){
		event.data.ref.remove()
	}, 10000)
	return null
});