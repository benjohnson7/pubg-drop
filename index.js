var fs = require('fs');
var express = require('express');
var app = express();
var privateKey = fs.readFileSync('cert/privatekey.key', 'utf8');
var certificate = fs.readFileSync('cert/certificate.crt', 'utf8');
var cred = {key: privateKey, cert: certificate};
var http = require('http').createServer(app);
var https = require('https').createServer(cred, app);
var path = require('path');

var staticPath = path.join(__dirname, '/public');
app.use(express.static(staticPath));

http.listen(3000, function(){
	console.log('listening on *:3000');
});
https.listen(3001, function(){
	console.log('listening on *:3001');
})

